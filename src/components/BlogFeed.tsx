import React from 'react';
import { Blog } from '../../typings';

type Props = {
  blogs: Blog[];
};

const BlogFeed: React.FC<Props> = ({ blogs }) => {
  const renderContent = (content: string) => {
    const urlPattern = /public\/uploads\/[^\s]+/g;
    const matches = content.match(urlPattern);
    if (matches) {
      const htmlString = matches.reduce((acc, match) => {
        const parts = acc.split(match);
        const imageUrl = match.replace('public', '');
        return `${parts[0]}<img src="${imageUrl}" alt="Embedded Image" />${parts.slice(1)}`;
      }, content);
      return <div dangerouslySetInnerHTML={{ __html: htmlString }} />;
    }
    return content;
  };

  return (
    <div>
      <br />
      <ul style={{ listStyle: 'none', padding: 0 }}>
        {blogs.map(blog => (
          <li key={blog.id}>
            <div
              style={{
                backgroundColor: 'white',
                borderRadius: '8px',
                padding: '16px',
                boxShadow: '0px 4px 12px rgba(0, 0, 0, 0.1)',
              }}
            >
              <div style={{ marginLeft: '6.25%', marginRight: '6.25%' }}>
                <h3 style={{ fontSize: '24px', fontWeight: 'bold', textAlign: 'center' }}>
                  {blog.title}
                </h3>
                {renderContent(blog.content)}
                <p style={{ fontWeight: 'bold', textAlign: 'right' }}>Author: {blog.author}</p>
                {blog.date && (
                  <p style={{ fontWeight: 'bold', textAlign: 'right' }}>
                    Published on: {new Date(blog.date).toLocaleDateString()}
                  </p>
                )}
              </div>
            </div>
            <br />
          </li>
        ))}
      </ul>
      <style jsx>{`
        @media (min-width: 768px) {
          li > div {
            width: 75%;
            margin-left: auto;
            margin-right: auto;
          }
        }
      `}</style>
    </div>
  );
};

export default BlogFeed;