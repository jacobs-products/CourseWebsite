import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { IProduct } from "../../typings";
import { addToBasket, removeFromBasket, selectItems } from "../slices/basketSlice";
import numeral from "numeral";

type Props = {
  product: IProduct;
};

const CheckoutProduct = ({ product }: Props) => {
  const { id, title, price, description, category, image, quantity } = product;
  const dispatch = useDispatch();
  
  // Call useSelector at the top level of the component
  const basketItems = useSelector(selectItems);

  const addItemToBasket = () => {
    let product: IProduct = {
      id,
      title,
      price,
      category,
      image,
      quantity,
    };
    if (description) {
      product.description = description;
    }
    
    // Check if there are enough items available before adding to basket
    const itemCount = basketItems.filter(item => item.id === id).length;
    
    if (itemCount < quantity) {
      // Send product id to Redux Store to remove from basket
      dispatch(addToBasket(product));
    } else {
      alert(`Can't add more than ${quantity} of this product to basket`);
    }
  };
  const removeItemFromBasket = () => {
    // Send product to Redux Store as a basket slice action
    dispatch(removeFromBasket({id}));
  };

  return (
    <div className="grid grid-cols-5 bg-white p-5 mb-4 shadow-md rounded-md">
      {/* Left */}
      <img
        className="object-contain col-span-1 h-24 md:h-40 lg:h-48"
        src={image}
        alt={title}
      />
      {/* Middle */}
      <div className="col-span-3 mx-5 my-auto">
        <p className="text-base md:text-lg font-semibold text-gray-800">{title}</p>
        {description && (
          <p className="text-xs md:text-sm my-2 line-clamp-3 text-gray-800">{description}</p>
        )}
        <p className="text-base md:text-lg font-bold text-gray-800">
          {numeral(price).format("$0,0.00")}
        </p>
      </div>
      {/* Right */}
      <div className="col-span-1 flex flex-col justify-between my-auto space-y-2">
        <button
          onClick={addItemToBasket}
          className="button bg-gray-500 hover:bg-black"
        >
          Add
        </button>
        <button
          onClick={removeItemFromBasket}
          className="button bg-gray-500 hover:bg-black"
        >
          Remove
        </button>
      </div>
    </div>
  );  
};

export default CheckoutProduct;