import { Bars3Icon, MagnifyingGlassIcon, ShoppingCartIcon } from "@heroicons/react/24/outline";
import { signIn, signOut, useSession } from "next-auth/react";
import Image from "next/image";
import { useRouter } from "next/router";
import { useSelector } from "react-redux";
import { selectItems } from "../slices/basketSlice";
import { useCategories } from '../hooks/useCategories';
import { useState } from "react";

const Header = () => {
  const [dropdownOpen, setDropdownOpen] = useState(false);
  const [searchQuery, setSearchQuery] = useState('');
  const { data: session } = useSession();
  const [openCategoryIndex, setOpenCategoryIndex] = useState<number | null>(null);
  const [openSubcategoryIndex, setOpenSubcategoryIndex] = useState<number | null>(null);
  const router = useRouter();
  const items = useSelector(selectItems);
  const categories = useCategories();

  const handleSearch = () => {
    router.push(`/search?q=${searchQuery}`);
  };

  return (
    <header style={{ position: 'relative', zIndex: 100 }} className="bg-white shadow-sm">
      <div className="container mx-auto flex items-center justify-between p-4 max-w-full flex-grow py-2">
        <div className="flex items-center flex-grow sm:flex-grow-0">
          <Image
            onClick={() => router.push('/')}
            className="cursor-pointer overflow-hidden mt-2"
            src="/linguapro.png"
            width={100}
            height={30}
            alt="LinguaPro"
          />
        </div>
        
        <div className="flex items-center space-x-4">
          <div className="hidden sm:flex items-center h-10 rounded-md flex-grow cursor-pointer flex items-center border rounded-md bg-gray-100">
            <input
              type="text"
              className="p-2 flex-grow rounded-l-md focus:outline-none bg-transparent"
              value={searchQuery}
              onChange={(event) => setSearchQuery(event.target.value)}
              placeholder="Search for courses..."
              onKeyDown={(event) => {
                if (event.key === "Enter") {
                  event.preventDefault();
                  handleSearch();
                }
              }}
            />
            <MagnifyingGlassIcon className="h-6 p-2 cursor-pointer border-l" onClick={handleSearch} />
          </div>

          {session ? (
            <div className="flex items-center space-x-4">
              <div
                onMouseEnter={() => setDropdownOpen(true)}
                onMouseLeave={() => setDropdownOpen(false)}
                className="relative text-gray-600 hover:underline cursor-pointer"
              >
                Hello, {session.user.name}
                {dropdownOpen && (
                  <div className="absolute top-full right-0 bg-white text-gray-600 p-2 rounded-md shadow-md">
                    <button onClick={() => signOut()} className="link hover:underline">
                      Sign Out
                    </button>
                  </div>
                )}
              </div>
              <div onClick={() => session && router.push('/orders')} className="cursor-pointer text-gray-600 hover:underline">
                Orders
              </div>
            </div>
          ) : (
            <button onClick={() => signIn()} className="text-gray-600 hover:underline">Sign In</button>
          )}

          <div onClick={() => router.push('/checkout')} className="relative flex items-center cursor-pointer">
            {items.length > 0 && (
              <span className="absolute top-0 -right-2 w-4 h-4 bg-gray-500 text-center rounded-full text-white font-bold">
                {items.length}
              </span>
            )}
            <ShoppingCartIcon className="h-6 text-gray-400 hover:text-black" />
            <span className="ml-2 text-gray-400 hover:text-black">Basket</span>
          </div>
        </div>
      </div>

      {/* Category and subcategory navigation */}
      <nav className="border-t bg-gray-100">
        <div className="container mx-auto flex items-center justify-start space-x-4 p-4">
          {categories.map((category, index) => (
            <div
              key={category.name}
              className="relative text-gray-600 hover:underline cursor-pointer"
              onMouseEnter={() => setOpenCategoryIndex(index)}
              onMouseLeave={() => setOpenCategoryIndex(null)}
              onTouchStart={() => setOpenCategoryIndex(index)}
            >
              <p
                onClick={() => router.push(`/category/${category.name}`)}
                style={{
                  WebkitTouchCallout: "none",
                  WebkitUserSelect: "none",
                  KhtmlUserSelect: "none",
                  MozUserSelect: "none",
                  msUserSelect: "none",
                  userSelect: "none",
                }}
              >
                {category.name}
              </p>
              {openCategoryIndex === index && category.subcategories.length > 0 && (
                <div className="absolute top-full left-1/2 transform -translate-x-1/2 bg-gray-100 text-gray-600 p-2 rounded-md shadow-md">
                  {category.subcategories.map((subcategory, index) => (
                    <div
                      key={subcategory.name}
                      className="relative"
                      onMouseEnter={() => setOpenSubcategoryIndex(index)}
                      onMouseLeave={() => setOpenSubcategoryIndex(null)}
                      onTouchStart={() => setOpenSubcategoryIndex(index)}
                    >
                      <p
                        className="link my-2 text-center hover:underline cursor-pointer"
                        onClick={() => router.push(`/category/${category.name}/${subcategory.name}`)}
                        style={{
                          WebkitTouchCallout: "none",
                          WebkitUserSelect: "none",
                          KhtmlUserSelect: "none",
                          MozUserSelect: "none",
                          msUserSelect: "none",
                          userSelect: "none",
                        }}
                      >
                        {subcategory.name}
                      </p>
                      {openSubcategoryIndex === index && subcategory.subsubcategories.length > 0 && (
                        <div className="my-2 text-center absolute top-0 left-full bg-gray-100 text-gray-600 p-2 rounded-md shadow-md">
                          {subcategory.subsubcategories.map((subsubcategory) => (
                            <p
                              key={subsubcategory}
                              className="link hover:underline cursor-pointer"
                              onClick={() => router.push(`/category/${category.name}/${subcategory.name}/${subsubcategory}`)}
                            >
                              {subsubcategory}
                            </p>
                          ))}
                        </div>
                      )}
                    </div>
                  ))}
                </div>
              )}
            </div>
          ))}
          <div onClick={() => router.push("/paidcourses")} className="text-gray-600 hover:underline cursor-pointer">
            My Courses
          </div>
          <div onClick={() => router.push("/contactus")} className="text-gray-600 hover:underline cursor-pointer">
            Contact Us
          </div>
        </div>
      </nav>

      {/* Mobile search bar */}
      <div className="md:hidden flex justify-center px-4 bg-white mb-2">
        <div className="flex items-center h-10 rounded-md flex-grow cursor-pointer bg-white">
          <input
            type="text"
            className="bg-white p-2 h-full w-6 flex-grow flex-shrink rounded-l-md focus:outline-none px-4"
            value={searchQuery}
            onChange={event => setSearchQuery(event.target.value)}
            onKeyDown={event => {
              if (event.key === 'Enter') {
                event.preventDefault();
                handleSearch();
              }
            }}
          />
          <MagnifyingGlassIcon className="h-6 p-2 cursor-pointer border-l" onClick={handleSearch} />
        </div>
      </div>
    </header>
  );
};

export default Header;