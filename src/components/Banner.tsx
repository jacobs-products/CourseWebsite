import Image from "next/image";
import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import CarouselOverlay from './CarouselOverlay';

type Props = {};

const Banner: React.FC<Props> = (props: Props) => {
  return (
    <div className="relative">
      <style jsx>{`
        .banner {
          height: 150vh;
        }

        /* Set a different height for screens with a width of 768px or more */
        @media (min-width: 768px) {
          .banner {
            height: 100vh;
          }
        }
      `}</style>
      <div className="absolute w-full bottom-0 z-20" />
      <Carousel
        autoPlay
        infiniteLoop
        showStatus={false}
        showIndicators={false}
        showThumbs={false}
        interval={10000}
        transitionTime={1000}
        stopOnHover={false}
      >
        <div className="relative banner">
          <Image
            layout="fill"
            objectFit="cover"
            src="/carousel1.png"
            alt="carousel1"
            priority
          />
        </div>
        <div className="relative banner">
          <Image
            layout="fill"
            objectFit="cover"
            src="/carousel2.png"
            alt="carousel2"
            priority
          />
        </div>
        <div className="relative banner">
          <Image
            layout="fill"
            objectFit="cover"
            src="/carousel3.png"
            alt="carousel3"
            priority
          />
        </div>
        <div className="relative banner">
          <Image
            layout="fill"
            objectFit="cover"
            src="/carousel4.png"
            alt="carousel4"
            priority
          />
        </div>
        <div className="relative banner">
          <Image
            layout="fill"
            objectFit="cover"
            src="/carousel5.png"
            alt="carousel5"
            priority
          />
        </div>
        <div className="relative banner">
          <Image
            layout="fill"
            objectFit="cover"
            src="/carousel6.png"
            alt="carousel6"
            priority
          />
        </div>
        <div className="relative banner">
          <Image
            layout="fill"
            objectFit="cover"
            src="/carousel7.png"
            alt="carousel7"
            priority
          />
        </div>
        <div className="relative banner">
          <Image
            layout="fill"
            objectFit="cover"
            src="/carousel8.png"
            alt="carousel8"
            priority
          />
        </div>
        <div className="relative banner">
          <Image
            layout="fill"
            objectFit="cover"
            src="/carousel9.png"
            alt="carousel9"
            priority
          />
        </div>
        <div className="relative banner">
          <Image
            layout="fill"
            objectFit="cover"
            src="/carousel10.png"
            alt="carousel20"
            priority
          />
        </div>
        <div className="relative banner">
          <Image
            layout="fill"
            objectFit="cover"
            src="/carousel11.png"
            alt="carousel11"
            priority
          />
        </div>
        <div className="relative banner">
          <Image
            layout="fill"
            objectFit="cover"
            src="/carousel12.png"
            alt="carousel12"
            priority
          />
        </div>
        <div className="relative banner">
          <Image
            layout="fill"
            objectFit="cover"
            src="/carousel13.png"
            alt="carousel13"
            priority
          />
        </div>
        <div className="relative banner">
          <Image
            layout="fill"
            objectFit="cover"
            src="/carousel14.png"
            alt="carousel14"
            priority
          />
        </div>
        <div className="relative banner">
          <Image
            layout="fill"
            objectFit="cover"
            src="/carousel15.png"
            alt="carousel15"
            priority
          />
        </div>
        <div className="relative banner">
          <Image
            layout="fill"
            objectFit="cover"
            src="/carousel16.png"
            alt="carousel16"
            priority
          />
        </div>
        <div className="relative banner">
          <Image
            layout="fill"
            objectFit="cover"
            src="/carousel17.png"
            alt="carousel17"
            priority
          />
        </div>
        <div className="relative banner">
          <Image
            layout="fill"
            objectFit="cover"
            src="/carousel18.png"
            alt="carousel18"
            priority
          />
        </div>
        <div className="relative banner">
          <Image
            layout="fill"
            objectFit="cover"
            src="/carousel19.png"
            alt="carousel19"
            priority
          />
        </div>
        <div className="relative banner">
          <Image
            layout="fill"
            objectFit="cover"
            src="/carousel20.png"
            alt="carousel20"
            priority
          />
        </div>
        <div className="relative banner">
          <Image
            layout="fill"
            objectFit="cover"
            src="/carousel21.png"
            alt="carousel21"
            priority
          />
        </div>
        <div className="relative banner">
          <Image
            layout="fill"
            objectFit="cover"
            src="/carousel22.png"
            alt="carousel22"
            priority
          />
        </div>
        <div className="relative banner">
          <Image
            layout="fill"
            objectFit="cover"
            src="/carousel23.png"
            alt="carousel23"
            priority
          />
        </div>
        <div className="relative banner">
          <Image
            layout="fill"
            objectFit="cover"
            src="/carousel24.png"
            alt="carousel24"
            priority
          />
        </div>
        <div className="relative banner">
          <Image
            layout="fill"
            objectFit="cover"
            src="/carousel25.png"
            alt="carousel25"
            priority
          />
        </div>
        <div className="relative banner">
          <Image
            layout="fill"
            objectFit="cover"
            src="/carousel26.png"
            alt="carousel25"
            priority
          />
        </div>
        <div className="relative banner">
          <Image
            layout="fill"
            objectFit="cover"
            src="/carousel27.png"
            alt="carousel25"
            priority
          />
        </div>
        <div className="relative banner">
          <Image
            layout="fill"
            objectFit="cover"
            src="/carousel28.png"
            alt="carousel25"
            priority
          />
        </div>
        <div className="relative banner">
          <Image
            layout="fill"
            objectFit="cover"
            src="/carousel29.png"
            alt="carousel25"
            priority
          />
        </div>
        <div className="relative banner">
          <Image
            layout="fill"
            objectFit="cover"
            src="/carousel30.png"
            alt="carousel25"
            priority
          />
        </div>
        <div className="relative banner">
          <Image
            layout="fill"
            objectFit="cover"
            src="/carousel31.png"
            alt="carousel25"
            priority
          />
        </div>
        <div className="relative banner">
          <Image
            layout="fill"
            objectFit="cover"
            src="/carousel32.png"
            alt="carousel25"
            priority
          />
        </div>
      </Carousel>
      <CarouselOverlay />
    </div>
  );
};

export default Banner;