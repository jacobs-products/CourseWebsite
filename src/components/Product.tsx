import React from "react";
import numeral from "numeral";
import { useDispatch, useSelector } from "react-redux";
import { useSession } from "next-auth/react";
import { IProduct, IModule } from "../../typings";
import { addToBasket, selectItems } from "../slices/basketSlice";

type Props = {
  product: IProduct;
};

const Product: React.FC<Props> = ({ product }: Props) => {
  const {
    id,
    title,
    price,
    description,
    category,
    image,
    quantity,
    modules,
    gradingPolicy,
    topicsCovered
  } = product;
  const dispatch = useDispatch();

  // Call useSelector at the top level of the component
  const basketItems = useSelector(selectItems);

  // Get session data
  const { data: session } = useSession();

  const addItemToBasket = () => {
    let product: IProduct = {
      id,
      title,
      price,
      category,
      image,
      quantity,
      description, // Show the whole description
    };

    // Check if there are enough items available before adding to basket
    const itemCount = basketItems.filter((item) => item.id === id).length;

    if (itemCount < quantity) {
      // Send product to Redux Store as a basket slice action
      dispatch(addToBasket({ ...product }));
    } else {
      alert(`Can't add more than ${quantity} of this product to basket`);
    }
  };

  if (!process.env.NEXT_PUBLIC_AUTHORIZED_EMAIL) {
    console.error(
      "NEXT_PUBLIC_AUTHORIZED_EMAIL environment variable is not set"
    );
    return null;
  }

  const authorizedEmails = process.env.NEXT_PUBLIC_AUTHORIZED_EMAIL.includes(
    ","
  )
    ? process.env.NEXT_PUBLIC_AUTHORIZED_EMAIL.split(",")
    : [process.env.NEXT_PUBLIC_AUTHORIZED_EMAIL];

  if (
    quantity <= 0 &&
    !authorizedEmails.includes(session?.user.email as string)
  )
    return null;
  
  return (
    <>
      <div className="relative flex flex-col items-center m-2 z-30 p-6 w-full md:w-1/2 lg:w-1/4 rounded-lg">
        <div style={{ backgroundColor: '#3d4a87' }} className="w-full rounded-t-lg p-6 relative">
          <p className="absolute top-2 right-2 text-xs italic text-white">
            {category}
          </p>
          <img
            className="object-contain w-full h-auto mx-auto"
            src={image}
            alt={title}
          />
          <h4 className="my-2 text-center text-white font-bold">{title}</h4>
        </div>
        <div className="w-full bg-white rounded-b-lg p-10">
          <h3 className="text-gray-800 text-center">Description</h3>
          {description && (
            <p className="text-xs my-1 text-center text-gray-800">{description}</p>
          )}
          <div className="mb-3 text-center text-gray-800">
            {numeral(price).format("$0,0.00")}
          </div>
          {/* <p className="text-sm my-1 text-center text-gray-800">
            Available Quantity: {quantity}
          </p> */}
          <button
            onClick={addItemToBasket}
            className="mt-auto button bg-gray-500 hover:bg-black"
          >
            Add to Basket
          </button>
        </div>
      </div>
    </>
  );
};

export default Product;