import Link from 'next/link';

type Props = {};

const Footer = (props: Props) => {
  return (
    <footer className="bg-black text-white p-10">
      <div className="flex justify-between">
        <div className="flex flex-col space-y-2">
          <h4 className="font-bold mb-2 text-white">Get to Know Us</h4>
          <ul className="text-white">
            <li>
              <Link href="/about">
                <a className="link">About Us</a>
              </Link>
            </li>
          </ul>
        </div>
      </div>
      <div className="flex space-x-2 mt-10" style={{ alignItems: 'flex-start' }}>
        <Link href="https://www.fiverr.com/thereconjacob/create-a-cheap-easy-to-use-ecommerce-website-with-zero-maintenance-costs">
          <a className="flex items-center">
            <p className="mr-2">This website was made by TheReconJacob from</p>
            <img
              src="https://freelogopng.com/images/all_img/1656739841old-fiverr-logo-white.png"
              alt="Fiverr"
              style={{ height: '18px', width: 'auto' }}
            />
          </a>
        </Link>
      </div>
    </footer>
  );    
};

export default Footer;