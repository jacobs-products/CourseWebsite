import Link from 'next/link';

type Props = {};

const CarouselOverlay: React.FC<Props> = (props: Props) => {
  return (
    <div className="absolute inset-0 bg-black bg-opacity-50 flex flex-col items-center justify-center">
      <p className="text-white text-lg font-semibold">EXCELLENCE IN EDUCATION</p>
      <br />
      <h2 className="text-white text-3xl font-bold">Welcome To LinguaPro Arabic Online Courses</h2>
      <br />
      <p className="text-white text-sm">Learn Arabic from a native speaker who is also a veteran highly qualified teacher</p>
      <br />
      <Link href="/contactus">
        <a className="mt-4 px-4 py-2 border border-white rounded-md text-white hover:bg-white hover:text-black">Contact Us</a>
      </Link>
    </div>
  );
};

export default CarouselOverlay;