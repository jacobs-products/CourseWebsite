import React from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useSession } from 'next-auth/react';
import { useProductContext } from 'components/context/ProductContext';
import Header from 'components/Header';
import { useFetchOrders } from '../hooks/UseFetchOrders';
import { useOrderContext } from '../components/context/OrderContext';

const PaidCourses: React.FC = () => {
  const router = useRouter();
  const { data: session } = useSession();
  useFetchOrders(session);
  const { orders } = useOrderContext();
  const { products } = useProductContext();

  // Get the titles of all the courses that the user has purchased
  const purchasedCourses = orders.map(order => order.title);

  // Filter the products to only include the courses that the user has purchased
  let paidCourses = products.filter(product => purchasedCourses.includes(product.title));

  const authorizedEmails = (process.env.NEXT_PUBLIC_AUTHORIZED_EMAIL as any).includes(",")
    ? (process.env.NEXT_PUBLIC_AUTHORIZED_EMAIL as any).split(",")
    : [process.env.NEXT_PUBLIC_AUTHORIZED_EMAIL];

  if (authorizedEmails.includes(session?.user.email as string)) {
    paidCourses = products;
  }

  return (
    <div className="bg-white min-h-screen">
      <Header />
      <main className="max-w-screen-2xl mx-auto p-4 md:p-8">
        <h1>My Courses</h1>
        <br />
        {paidCourses.length > 0 ? (
          <ul>
            {paidCourses.map((course) => (
              <li key={course.id}>
                <Link href={`/courses/${encodeURIComponent(course.title)}`}>
                  <button style={{ backgroundColor: '#3d4a87' }} onMouseOver={(e) => e.currentTarget.style.backgroundColor = '#5a6da8'} className="text-white px-4 py-2 rounded mb-4">{course.title}</button>
                </Link>
                <br />
              </li>
            ))}
          </ul>
        ) : (
          <p>You haven't purchased any courses.</p>
        )}
      </main>
    </div>
  );
};

export default PaidCourses;