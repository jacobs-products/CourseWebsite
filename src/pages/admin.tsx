import dynamic from 'next/dynamic';
import React, { useState } from 'react';
import GoTrue from 'gotrue-js';

const CMS = dynamic(
  () =>
    import('netlify-cms-app').then((cms) => {
      console.log('Initializing Netlify CMS...');
      cms.init();
      console.log('Netlify CMS initialized successfully!');
      return () => null;
    }),
  { ssr: false, loading: () => <div>Loading...</div> }
);

const AdminPage = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [message, setMessage] = useState('');

  const auth = new GoTrue({
    APIUrl: `${process.env.HOST}/.netlify/identity`,
    audience: '',
    setCookie: false,
  });

  const handleRegister = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    try {
      await auth.signup(email, password);
      setMessage('Registration successful! Please check your email to confirm your account.');
    } catch (error) {
      setMessage(`Registration failed: ${(error as Error).message}`);
    }
  };

  return (
    <div>
      <CMS />
      Netlify CMS Admin
      <h2>Register</h2>
      <form onSubmit={handleRegister}>
        <label htmlFor="email">Email:</label>
        <input
          type="email"
          id="email"
          value={email}
          onChange={(event) => setEmail(event.target.value)}
        />
        <br />
        <label htmlFor="password">Password:</label>
        <input
          type="password"
          id="password"
          value={password}
          onChange={(event) => setPassword(event.target.value)}
        />
        <br />
        <button type="submit">Register</button>
      </form>
      {message && <p>{message}</p>}
    </div>
  );
};

export default AdminPage;