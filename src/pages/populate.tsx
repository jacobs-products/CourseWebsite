import { useState } from "react";
import { GetServerSidePropsContext } from "next";
import { getSession, useSession } from "next-auth/react";
import Head from "next/head";
import { IModule, IProduct, ISection, ISession } from "../../typings";
import Header from "../components/Header";
import { collection, getDocs, addDoc, updateDoc, deleteDoc, query, where, serverTimestamp, doc } from 'firebase/firestore';
import db from '../../firebase';
import { useProductContext } from "components/context/ProductContext";
import dynamic from "next/dynamic";
const ReactQuill = dynamic(() => import("react-quill"), { ssr: false });
import 'react-quill/dist/quill.snow.css';
import { Quiz } from 'react-quiz-component';

const Populate = (props: any) => {
  const [showQuillEditor, setShowQuillEditor] = useState(false);
  const [quillContent, setQuillContent] = useState('');

  const { products, loading, error } = useProductContext();
  const { data: session } = useSession();
  
  if (!process.env.NEXT_PUBLIC_AUTHORIZED_EMAIL) {
    console.error('NEXT_PUBLIC_AUTHORIZED_EMAIL environment variable is not set');
    return null;
  }
  
  const authorizedEmails = process.env.NEXT_PUBLIC_AUTHORIZED_EMAIL.includes(',')
    ? process.env.NEXT_PUBLIC_AUTHORIZED_EMAIL.split(',')
    : [process.env.NEXT_PUBLIC_AUTHORIZED_EMAIL];

  // Check if the logged-in user is authorized
  const isAuthorized = authorizedEmails.includes(session?.user.email as string);

  const [newProduct, setNewProduct] = useState<IProduct>({
    id: '',
    title: '',
    price: 0,
    description: '',
    category: '',
    image: '',
    quantity: 1,
    modules: [{
      title: "",
      content: "",
      isSetUp: false,
      order: 0,
      sections: []
    }]
  });  

  // State to keep track of the edited product fields
  const [editedProducts, setEditedProducts] = useState<IProduct[]>(products);

  const [removedProducts, setRemovedProducts] = useState<IProduct[]>([]);

  const [selectedFile, setSelectedFile] = useState<File | null>(null);

  const [selectedFiles, setSelectedFiles] = useState<(File | null)[]>(new Array(editedProducts.length).fill(null));

  const [topicsCovered, setTopicsCovered] = useState(newProduct.topicsCovered || [""]);
  
  const [newSectionData, setNewSectionData] = useState<any>({});
  
  const [editedSectionData, setEditedSectionData] = useState<any>({});
  
  const [newSectionTypes, setNewSectionTypes] = useState<string[]>([]);

  const [editedSectionType, setEditedSectionType] = useState<string>('');

  const [newQuestionType, setNewQuestionType] = useState<string>('');

  const [newChoices, setNewChoices] = useState<string[]>(['']);
  
  const [newCorrectAnswers, setNewCorrectAnswers] = useState<string[]>([]);

  const [newQuestions, setNewQuestions] = useState<any[]>([]);
  
  const [newQuestionData, setNewQuestionData] = useState<any>({});

  const [newProductButtonText, setNewProductButtonText] = useState("");

  const [editedProductsButtonText, setEditedProductsButtonText] = useState("");

  // Add a new state variable to keep track of the currently edited module
  const [editedModule, setEditedModule] = useState<{
    productIndex: number;
    moduleIndex: number;
  } | null>(null);

  const [editedSection, setEditedSection] = useState<{
    productIndex: number;
    moduleIndex: number;
    sectionIndex: number;
  } | null>(null);

  const [editedQuestion, setEditedQuestion] = useState<{
    productIndex: number;
    moduleIndex: number;
    sectionIndex: number;
    questionIndex: number;
  } | null>(null);

  const [editedChoice, setEditedChoice] = useState<{
    productIndex: number;
    moduleIndex: number;
    sectionIndex: number;
    questionIndex: number;
    choiceIndex: number;
  } | null>(null);  
  
  const handleUpdateTopic = (index: number, value: string) => {
    const newTopicsCovered = [...topicsCovered];
    newTopicsCovered[index] = value;
    setTopicsCovered(newTopicsCovered);
    handleUpdateNewProductField("topicsCovered", newTopicsCovered);
  };
  
  const handleAddTopic = () => {
    setTopicsCovered([...topicsCovered, ""]);
  };

  const handleFileChange = (file: File) => {
    setSelectedFile(file);
  };

  const handleEditContent = (content: string) => {
    setQuillContent(content);
    setShowQuillEditor(true);
  };

  function handleUpdateNewModuleField(index: number, field: keyof IModule, value: any) {
    const modulesCopy = [...(newProduct.modules || [])];
    (modulesCopy[index] as any)[field] = value;
    setNewProduct(prevProduct => ({ ...prevProduct, modules: modulesCopy }));
  }

  function handleAddNewModule() {
    const newModule: IModule = {
      title: "",
      content: "",
      isSetUp: false,
      order: 0, // add order field here
    };
    setNewProduct((prevProduct) => ({
      ...prevProduct,
      modules: [...(prevProduct.modules || []), newModule],
    }));
  }  

  // Add a new function to handle updating the new section data
  const handleUpdateNewSectionData = (field: string, value: any) => {
    setNewSectionData((prevState: any) => ({
      ...prevState,
      [field]: value,
    }));
  };

  const handleUpdateNewSectionType = (moduleIndex: number, value: string) => {
    setNewSectionTypes(prevState => {
      const newSectionTypesCopy = [...prevState];
      newSectionTypesCopy[moduleIndex] = value;
      return newSectionTypesCopy;
    });
  };

  const handleUpdateEditedSectionData = (field: string, value: any) => {
    setEditedSectionData((prevState: any) => ({
      ...prevState,
      [field]: value,
    }));
  };

  const handleUpdateNewQuestionData = (field: string, value: any) => {
    setNewQuestionData((prevState: any) => ({
      ...prevState,
      [field]: value,
    }));
  };
  
  const handleAddNewQuestion = () => {
    let newQuestion: any;
    if (newQuestionData.questionType === 'multipleChoice') {
      newQuestion = {
        ...newQuestionData,
        questionType: newQuestionData.questionType,
        choices: newChoices,
        correctAnswers: newCorrectAnswers,
      };
    } else if (newQuestionData.questionType === 'text') {
      newQuestion = {
        ...newQuestionData,
        questionType: newQuestionData.questionType,
      };
    }
    setNewQuestions((prevState) => [...prevState, newQuestion]);
    
    // Reset the state of newQuestionData, newChoices, and newCorrectAnswers
    setNewQuestionData({});
    setNewChoices(['']);
    setNewCorrectAnswers([]);
    setNewQuestionType('');
  };
  
  const handleAddNewSection = (moduleIndex: number) => {
    const modulesCopy = [...(newProduct.modules || [])];
    if (!modulesCopy[moduleIndex].sections) {
      modulesCopy[moduleIndex].sections = [];
    }
    (modulesCopy[moduleIndex] as any).sections.push({
      title: newSectionData.title,
      type: newSectionTypes[moduleIndex],
      data: {
        ...newSectionData,
        questions: newQuestions,
      },
      order: newSectionData.order,
    });
    setNewProduct((prevProduct) => ({ ...prevProduct, modules: modulesCopy }));
    
    // Reset the state of newSectionData, newSectionTypes, newQuestions, newQuestionData, newChoices, and newCorrectAnswers
    setNewSectionData({});
    setNewSectionTypes(prevState => {
      const newSectionTypesCopy = [...prevState];
      newSectionTypesCopy[moduleIndex] = '';
      return newSectionTypesCopy;
    });
    setNewQuestions([]);
    setNewQuestionData({});
    setNewChoices(['']);
    setNewCorrectAnswers([]);
  
    // Reset the state of quillContent
    setQuillContent('');
  };  

  function handleAddEditedModule(productIndex: number) {
    setEditedProducts((prevProducts) => {
      const updatedProducts = [...prevProducts];
      if (!updatedProducts[productIndex].modules) {
        updatedProducts[productIndex].modules = [];
      }
      (updatedProducts[productIndex] as any).modules.push({
        title: "",
        content: "",
        isSetUp: false,
        order: 0,
      });
      return updatedProducts;
    });
  }

  const handleAddEditedSection = (productIndex: number, moduleIndex: number) => {
    setEditedProducts((prevProducts) => {
      const updatedProducts = [...prevProducts];
      if (!updatedProducts[productIndex].modules) {
        updatedProducts[productIndex].modules = [];
      }
      if (!(updatedProducts[productIndex] as any).modules[moduleIndex].sections) {
        (updatedProducts[productIndex] as any).modules[moduleIndex].sections = [];
      }
      (updatedProducts[productIndex] as any).modules[moduleIndex].sections.push({
        title: "",
        type: "",
        data: { questions: [] },
        order: 0,
      });
      return updatedProducts;
    });
  };  


  const handleUpdateChoice = (index: number, value: string) => {
    const newChoicesCopy = [...newChoices];
    newChoicesCopy[index] = value;
    setNewChoices(newChoicesCopy);
  };
  
  const handleAddChoice = () => {
    setNewChoices([...newChoices, '']);
  };

  const handleUpdateCorrectAnswers = (choice: string, checked: boolean) => {
    if (checked) {
      setNewCorrectAnswers((prevState) => [...prevState, choice]);
    } else {
      setNewCorrectAnswers((prevState) => prevState.filter((answer) => answer !== choice));
    }
  };  

  const handleUpdateSection = (
    productIndex: number,
    moduleIndex: number,
    sectionIndex: number,
    field: keyof ISection,
    value: any
  ) => {
    setEditedProducts((prevProducts) => {
      const updatedProducts = [...prevProducts];
      if (!updatedProducts[productIndex].modules) {
        updatedProducts[productIndex].modules = [];
      }
      if (!(updatedProducts[productIndex] as any).modules[moduleIndex].sections) {       
        (updatedProducts[productIndex] as any).modules[moduleIndex].sections = [];
        if (field === 'data' && value.questions) {
          ((updatedProducts[productIndex] as any).modules[moduleIndex].sections as any)[
            sectionIndex
          ].data.questions = value.questions;
        } 
      }
      ((updatedProducts[productIndex] as any).modules[moduleIndex].sections as any)[
        sectionIndex
      ][field] = value;
      return updatedProducts;
    });
  };
  
  function handleUpdateModuleField(productIndex: number, moduleIndex: number, field: keyof IModule, value: any) {
    setEditedProducts(prevProducts => {
        const updatedProducts = [...prevProducts];
        if (!updatedProducts[productIndex].modules) {
            updatedProducts[productIndex].modules = [];
        }
        (updatedProducts[productIndex].modules as any)[moduleIndex][field] = value;
        return updatedProducts;
    });
  }

  const handleFilesChange = (index: number, file: File) => {
    setSelectedFiles(prevState => {
      const newSelectedFiles = [...prevState];
      newSelectedFiles[index] = file;
      return newSelectedFiles;
    });
  };  

  // Function to handle updating a field in the new product form
  const handleUpdateNewProductField = (
    field: keyof IProduct,
    value: string | number | boolean | string[]
  ) => {
    setNewProduct((prevState) => ({
      ...prevState,
      [field]: value,
    }));
  }

  // Function to handle updating a product field
  const handleUpdateProductField = (index: number, field: keyof IProduct, value: string | number) => {
    setEditedProducts(prevState => {
      const newProducts = [...prevState];
      (newProducts[index] as any)[field] = value;
      return newProducts;
    });
  }

  const handleRemoveProduct = (productId: string) => {
    setEditedProducts(prevState => {
      const newProducts = prevState.filter(product => product.id !== productId);
      const removedProduct = prevState.find(product => product.id === productId);
      if (removedProduct) {
        setRemovedProducts(prevState => [...prevState, removedProduct]);
      }
      return newProducts;
    });
  }

  // Function to handle submitting the new product form
  const handleSubmitNewProduct = async () => {

    setNewProductButtonText("Please wait 20s.");
    let counter = 20;
    const intervalId = setInterval(() => {
      counter--;
      setNewProductButtonText(`Please wait ${counter}s.`);
      if (counter === 0) {
        clearInterval(intervalId);
        setNewProductButtonText("Finished, please refresh the page a couple of times until the changes appear.");
      }
    }, 1000);

    // Upload the selected file to ImgBB using their API and get the resulting image URL
    let imageUrl = '';
    if (selectedFile) {
      const formData = new FormData();
      formData.append('image', selectedFile);
      const response = await fetch(`https://api.imgbb.com/1/upload?key=${process.env.NEXT_PUBLIC_IMGBB_API_KEY}`, {
        method: 'POST',
        body: formData,
      });
      const data = await response.json();
      imageUrl = data.data.url;
    }
  
    // Query the products collection to get all existing products
    const querySnapshot = await getDocs(collection(db, 'products'));
    // Find the highest id value among the existing products
    let maxId = 0;
    querySnapshot.forEach(doc => {
      const product = doc.data() as IProduct;
      const id = parseInt(product.id);
      if (id > maxId) {
        maxId = id;
      }
    });
    // Generate a new unique id by incrementing the highest id value by 1
    const newId = (maxId + 1).toString();
    // Add a new document to the products collection with the generated id
    await addDoc(collection(db, 'products'), {
      ...newProduct,
      id: newId,
      image: imageUrl,
      description: newProduct.description ?? '', // Set description to an empty string if it is undefined
      quantity: newProduct.quantity ?? 1, // Set quantity to 0 if it is undefined
      subcategory: newProduct.subcategory ?? '', // Set subcategory to an empty string if it is undefined
      subsubcategory: newProduct.subsubcategory ?? '', // Set subsubcategory to an empty string if it is undefined
      courseSchedule: newProduct.courseSchedule ?? '', // Set courseSchedule to an empty string if it is undefined
      modules: newProduct.modules ?? [], // Set modules to an empty array if it is undefined
      gradingPolicy: newProduct.gradingPolicy ?? '', // Set gradingPolicy to an empty object if it is undefined
      topicsCovered: newProduct.topicsCovered ?? [], // Set topicsCovered to an empty array if it is undefined
    });
    // Clear the input fields and selected file
    setNewProduct({
      id: '',
      title: '',
      price: 0,
      description: '',
      category: '',
      image: '',
      quantity: 1,
      subcategory: '',
      subsubcategory: '',
      courseSchedule: '',
      modules: [],
      gradingPolicy: '',
      topicsCovered: [],
    });
    setSelectedFile(null);
  
    // Update the last updated timestamp for the products collection
    const lastUpdatedRef = collection(db, 'lastUpdated');
    const lastUpdatedQuery = query(lastUpdatedRef, where('type', '==', 'products'));
    const lastUpdatedSnapshot = await getDocs(lastUpdatedQuery);
    const lastUpdatedDocRef = lastUpdatedSnapshot.docs[0]?.ref;
    await updateDoc(lastUpdatedDocRef, { timestamp: serverTimestamp() });
  };  

  // Function to handle submitting the changes
  const handleSubmitChanges = async () => {
    
    setEditedProductsButtonText("Please wait 20s.");
    let counter = 20;
    const intervalId = setInterval(() => {
      counter--;
      setEditedProductsButtonText(`Please wait ${counter}s.`);
      if (counter === 0) {
        clearInterval(intervalId);
        setEditedProductsButtonText("Finished, please refresh the page a couple of times until the changes appear.");
      }
    }, 1000);

    // Loop through the edited products
    for (const [index, product] of editedProducts.entries()) {
      // Upload the selected file to ImgBB using their API and get the resulting image URL
      let imageUrl = product.image;
      if (selectedFiles[index]) {
        const formData = new FormData();
        formData.append('image', selectedFiles[index] as File);
        const response = await fetch(`https://api.imgbb.com/1/upload?key=${process.env.NEXT_PUBLIC_IMGBB_API_KEY}`, {
          method: 'POST',
          body: formData,
        });
        const data = await response.json();
        imageUrl = data.data.url;
      }
  
      // Query the products collection for a document with a matching product.id field
      const querySnapshot = await getDocs(query(collection(db, 'products'), where('id', '==', product.id)));
      // Get the first document from the query result (there should only be one)
      const docSnapshot = querySnapshot.docs[0];
      if (docSnapshot) {
        // Get a reference to the product document
        const productRef = docSnapshot.ref;
        // Update the product document with the new information
        await updateDoc(productRef, {
          title: product.title,
          price: product.price,
          description: product.description ?? '', // Set description to an empty string if it is undefined
          category: product.category,
          image: imageUrl,
          quantity: product.quantity ?? 1, // Set quantity to 0 if it is undefined
          subcategory: product.subcategory ?? '', // Set subcategory to an empty string if it is undefined
          subsubcategory: product.subsubcategory ?? '', // Set subsubcategory to an empty string if it is undefined
          courseSchedule: product.courseSchedule ?? '', // Set courseSchedule to an empty string if it is undefined
          modules: product.modules ?? [], // Set modules to an empty array if it is undefined
          gradingPolicy: product.gradingPolicy ?? '', // Set gradingPolicy to an empty object if it is undefined
          topicsCovered: product.topicsCovered ?? [], // Set topicsCovered to an empty array if it is undefined
        });        
      }
    }
    // Loop through the removed products
    for (const product of removedProducts) {
      // Query the products collection for a document with a matching product.id field
      const querySnapshot = await getDocs(query(collection(db, 'products'), where('id', '==', product.id)));
      // Get the first document from the query result (there should only be one)
      const docSnapshot = querySnapshot.docs[0];
      if (docSnapshot) {
        // Get a reference to the product document
        const productRef = docSnapshot.ref;
        // Delete the product document
        await deleteDoc(productRef);
      } else {
        console.log(`No document found with id ${product.id}`);
      }
    }
  
    // Update the last updated timestamp for the products collection
    const lastUpdatedRef = collection(db, 'lastUpdated');
    const lastUpdatedQuery = query(lastUpdatedRef, where('type', '==', 'products'));
    const lastUpdatedSnapshot = await getDocs(lastUpdatedQuery);
    const lastUpdatedDocRef = lastUpdatedSnapshot.docs[0]?.ref;
    await updateDoc(lastUpdatedDocRef, { timestamp: serverTimestamp() });
  };  

  return (
    <div className="min-h-screen">
      <Header />
      {isAuthorized ? (
        // Render page content for authorized users
        <main className="max-w-screen-2xl mx-auto">
          {/* Header */}
          <Head>
            <title>Populate Products</title>
          </Head>
          <h2>When adding or editing a product, please allow roughly 20 seconds before navigating away from this page.</h2>
          {/* New product form */}
          <div className="m-5 bg-white z-30 p-10">
            <h2>New Product</h2>
            {/* Render input fields for each product field (except id) */}
            <label>Title: *</label>
            <input
              type="text"
              value={newProduct.title}
              onChange={e => handleUpdateNewProductField('title', e.target.value)}
              className="border border-gray-300 rounded-md p-1"
            />
            <br />
            <br />
            <label>Price: *</label>
            <input
              type="number"
              value={newProduct.price}
              onChange={e => handleUpdateNewProductField('price', parseFloat(e.target.value))}
              className="border border-gray-300 rounded-md p-1"
            />
            <br />
            <label>Description:</label>
            <input
              type="text"
              value={newProduct.description}
              onChange={e => handleUpdateNewProductField('description', e.target.value)}
              className="border border-gray-300 rounded-md p-1"
            />
            <br />
            <label>Category: *</label>
            <input
              type="text"
              value={newProduct.category}
              onChange={e => handleUpdateNewProductField('category', e.target.value)}
              className="border border-gray-300 rounded-md p-1"
            />
            <br />
            {/* Input field for subcategory */}
            <label>Subcategory:</label>
            <input
              type="text"
              value={newProduct.subcategory}
              onChange={e => handleUpdateNewProductField('subcategory', e.target.value)}
              className="border border-gray-300 rounded-md p-1"
            />
            <br />
            {/* Input field for subsubcategory */}
            <label>Sub-subcategory:</label>
            <input
              type="text"
              value={newProduct.subsubcategory}
              onChange={e => handleUpdateNewProductField('subsubcategory', e.target.value)}
              className="border border-gray-300 rounded-md p-1"
            />
            <br />
            {/* Input field for courseSchedule */}
            <label>Course Schedule:</label>
            <input
              type="text"
              value={newProduct.courseSchedule}
              onChange={e => handleUpdateNewProductField('courseSchedule', e.target.value)}
              className="border border-gray-300 rounded-md p-1"
            />
            <br />
            {/* Input field for gradingPolicy */}
            <label>Grading Policy:</label>
            <input
              type="text"
              value={newProduct.gradingPolicy}
              onChange={e => handleUpdateNewProductField('gradingPolicy', e.target.value)}
              className="border border-gray-300 rounded-md p-1"
            />
            <br />
            {/* Input fields for topicsCovered */}
            <label>Topics Covered:</label>
            {topicsCovered.map((topic, index) => (
              <input
                key={index}
                type="text"
                value={topic}
                onChange={e => handleUpdateTopic(index, e.target.value)}
                className="border border-gray-300 rounded-md p-1"
              />
            ))}
            <button onClick={handleAddTopic} className="bg-gray-600 text-white px-4 py-2 rounded-md">Add Topic</button>
            <br />
            {/* Modules/Modules */}
            <div>
              <h3>Modules</h3>
              {newProduct.modules && newProduct.modules.length > 0 ? (
                newProduct.modules.map((module, moduleIndex) => (
                  <div key={moduleIndex}>
                    {/* ... */}
                    <label>Module Title:{moduleIndex !== (newProduct.modules as any).length - 1 ? ' ' : ''}</label>
                    <input
                      type="text"
                      onChange={(e) =>
                        handleUpdateNewModuleField(moduleIndex, "title", e.target.value)
                      }
                      className={
                        moduleIndex === (newProduct.modules as any).length - 1
                          ? "border border-gray-300 rounded-md p-1"
                          : ""
                      }
                      readOnly={moduleIndex !== (newProduct.modules as any).length - 1}
                    />
                    <br />
                    <label>Module Content:{moduleIndex !== (newProduct.modules as any).length - 1 ? ' ' : ''}</label>
                    <input
                      type="text"
                      onChange={(e) =>
                        handleUpdateNewModuleField(moduleIndex, "content", e.target.value)
                      }
                      className={
                        moduleIndex === (newProduct.modules as any).length - 1
                          ? "border border-gray-300 rounded-md p-1"
                          : ""
                      }
                      readOnly={moduleIndex !== (newProduct.modules as any).length - 1}
                    />
                    <br />
                    <label>Is Set Up:{moduleIndex !== (newProduct.modules as any).length - 1 ? ' ' : ''}</label>
                    <input
                      type="checkbox"
                      onChange={(e) =>
                        handleUpdateNewModuleField(moduleIndex, "isSetUp", e.target.checked)
                      }
                      disabled={moduleIndex !== (newProduct.modules as any).length - 1}
                    />
                    <br />
                    <label>Module Order:{moduleIndex !== (newProduct.modules as any).length - 1 ? ' ' : ''}</label>
                    <input
                      type="number"
                      onChange={(e) =>
                        handleUpdateNewModuleField(moduleIndex, "order", parseInt(e.target.value))
                      }
                      className={
                        moduleIndex === (newProduct.modules as any).length - 1
                          ? "border border-gray-300 rounded-md p-1"
                          : ""
                      }
                      readOnly={moduleIndex !== (newProduct.modules as any).length - 1}
                    />
                    <br />
                    <h3>Sections</h3>
                    {module.sections && module.sections.length > 0 ? (
                      module.sections.map((section, sectionIndex) => (
                        <div key={sectionIndex}>
                          <p>Title: {section.title}</p>
                          <p>Type: {section.type}</p>
                          <p>Order: {section.order}</p>
                          {section.type === 'video' && <p>Video URL: {section.data.url}</p>}
                          {section.type === 'content' && <p>Content stored</p>}
                          {section.type === 'questions' && <p>Questions stored</p>}
                        </div>
                      ))
                    ) : (
                      <p>No sections added yet.</p>
                    )}
                    <br />
                    <label>Section Type:</label>
                    <select
                      value={newSectionTypes[moduleIndex] || ''}
                      onChange={(e) => handleUpdateNewSectionType(moduleIndex, e.target.value)}
                    >
                      <option value="">Select a type</option>
                      <option value="video">Video</option>
                      <option value="content">Content</option>
                      <option value="questions">Questions</option>
                    </select>
                    <br />
                    {newSectionTypes[moduleIndex] && 
                      <>
                      <label>Section Title:</label>
                      <input
                        type="text"
                        onChange={(e) =>
                          handleUpdateNewSectionData("title", e.target.value)
                        }
                        className="border border-gray-300 rounded-md p-1"
                      />
                      <br />
                      <label>Section Order:</label>
                      <input
                        type="number"
                        onChange={(e) =>
                          handleUpdateNewSectionData("order", parseInt(e.target.value))
                        }
                        className="border border-gray-300 rounded-md p-1"
                      />
                      <br />
                      </>
                    }
                    {newSectionTypes[moduleIndex] === 'video' && (
                      <>
                        <label>Video URL:</label>
                        <input
                          type="text"
                          onChange={(e) => handleUpdateNewSectionData('url', e.target.value)}
                          className="border border-gray-300 rounded-md p-1"
                        />
                        <br />
                      </>
                    )}
                    {newSectionTypes[moduleIndex] === 'content' && (
                      <>
                        <label>Content:</label>
                        <ReactQuill value={quillContent} onChange={setQuillContent} theme="snow" />
                        <button onClick={() => handleUpdateNewSectionData('content', quillContent)} className="bg-gray-600 text-white px-4 py-2 rounded-md">
                          Save Content
                        </button>
                        <br />
                      </>
                    )}
                    {newSectionTypes[moduleIndex] === 'questions' && (
                      <>
                        {newQuestions.map((question, index) => (
                          <div key={index}>
                            <p>Question {index + 1}:</p>
                            <p>Type: {question.questionType}</p>
                            <p>Question: {question.question}</p>
                            {question.questionType === 'multipleChoice' && (
                              <>
                                <p>Choices:</p>
                                <ul>
                                  {question.choices.map((choice: string, index: number) => (
                                    <li key={index}>{choice}</li>
                                  ))}
                                </ul>
                                <p>Correct Answers:</p>
                                <ul>
                                  {question.correctAnswers.map((answer: string, index: number) => (
                                    <li key={index}>{answer}</li>
                                  ))}
                                </ul>
                              </>
                            )}
                            {question.questionType === 'text' && <p>Answer: {question.answer}</p>}
                          </div>
                        ))}
                        <br />
                        <label>Question Type:</label>
                        <select 
                          value={newQuestionData.questionType || ''}
                          onChange={(e) => handleUpdateNewQuestionData('questionType', e.target.value)}
                        >
                          <option value="">Select a type</option>
                          <option value="multipleChoice">Multiple Choice</option>
                          <option value="text">Text</option>
                        </select>
                        <br />
                        {newQuestionData.questionType === 'multipleChoice' && (
                          <>
                            <label>Question:</label>
                            <input
                              type="text"
                              onChange={(e) => handleUpdateNewQuestionData('question', e.target.value)}
                              className="border border-gray-300 rounded-md p-1"
                            />
                            <br />
                            <label>Choices:</label>
                            {newChoices.map((choice, index) => (
                              <>
                                <input
                                  key={index}
                                  type="text"
                                  value={choice}
                                  onChange={(e) => handleUpdateChoice(index, e.target.value)}
                                  className="border border-gray-300 rounded-md p-1"
                                />
                                <input
                                  type="checkbox"
                                  onChange={(e) => handleUpdateCorrectAnswers(choice, e.target.checked)}
                                />
                                <br />
                              </>
                            ))}
                            <button onClick={handleAddChoice} className="bg-gray-900 text-white px-4 py-2 rounded-md">Add Choice</button>
                            <br />
                          </>
                        )}
                        {newQuestionData.questionType === 'text' && (
                          <>
                            <label>Question:</label>
                            <input
                              type="text"
                              onChange={(e) => handleUpdateNewQuestionData('question', e.target.value)}
                              className="border border-gray-300 rounded-md p-1"
                            />
                            <br />
                            <label>Answer:</label>
                            <input
                              type="text"
                              onChange={(e) => handleUpdateNewQuestionData('answer', e.target.value)}
                              className="border border-gray-300 rounded-md p-1"
                            />
                            <br />
                          </>
                        )}
                        <button onClick={handleAddNewQuestion} className="bg-gray-800 text-white px-4 py-2 rounded-md">Add Question</button>
                        <br />
                      </>
                    )}
                    {/* Add more input fields here for other section types */}
                    <button onClick={() => handleAddNewSection(moduleIndex)} className="bg-gray-600 text-white px-4 py-2 rounded-md">Add Section</button>
                  </div>
                ))
              ) : (
                <p>No modules added yet.</p>
              )}
              <button
                onClick={() =>
                  setNewProduct({
                    ...newProduct,
                    modules: [
                      ...(newProduct.modules || []),
                      { title: "", content: "", isSetUp: false, order: 0 },
                    ],
                  })
                }
                className="bg-gray-500 text-white px-4 py-2 rounded-md"
              >
                Add Module
              </button>
          </div>
            <br />
            <label>Image: *</label>
            <input
              type="file"
              onChange={e => {
                if (e.target.files) {
                  handleFileChange(e.target.files[0]);
                }
              }}
              className="border border-gray-300 rounded-md p-1"
            />
            <br />
            {/* <label>Quantity: *</label>
            <input
              type="number"
              value={newProduct.quantity}
              onChange={e => handleUpdateNewProductField('quantity', parseInt(e.target.value))}
              className="border border-gray-300 rounded-md p-1"
            /> */}
            {/* Submit button for New Product */}
            <button
              onClick={() => {
                if (
                  newProduct.title &&
                  newProduct.price &&
                  newProduct.category &&
                  (newProduct.image || selectedFile) &&
                  (newProduct.quantity || newProduct.quantity === 0)
                ) {
                  handleSubmitNewProduct();
                } else {
                  alert('Please fill in all required fields');
                }
              }}
              className="mt-auto button mx-auto mb-10"
            >
              Submit New Product
            </button>
            <br />
            <p>{newProductButtonText}</p>
          </div>
          {/* Product editor */}
          <div className="grid grid-flow-row-dense md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4">
          {editedProducts.map((product, index) => (
            <div key={product.id} className="relative flex flex-col m-5 bg-white z-30 p-10">
              {/* X button to remove product */}
              <button onClick={() => handleRemoveProduct(product.id)} className="absolute top-2 right-2 text-lg font-bold">
                X
              </button>
              {/* Render input fields for each product field (except id) */}
              <h2>Edit Product</h2>
              <label>Title: *</label>
              <input
                type="text"
                value={product.title}
                onChange={e => handleUpdateProductField(index, 'title', e.target.value)}
                className="border border-gray-300 rounded-md p-1"
              />
              <br />
              <label>Price: *</label>
              <input
                type="number"
                value={product.price}
                onChange={e => handleUpdateProductField(index, 'price', parseFloat(e.target.value))}
                className="border border-gray-300 rounded-md p-1"
              />
              <br />
              <label>Description:</label>
              <input
                type="text"
                value={product.description}
                onChange={e => handleUpdateProductField(index, 'description', e.target.value)}
                className="border border-gray-300 rounded-md p-1"
              />
              <br />
              <label>Category: *</label>
              <input
                type="text"
                value={product.category}
                onChange={e => handleUpdateProductField(index, 'category', e.target.value)}
                className="border border-gray-300 rounded-md p-1"
              />
              <br />
              {/* Input field for subcategory */}
              <label>Subcategory:</label>
              <input
                type="text"
                value={product.subcategory}
                onChange={e => handleUpdateProductField(index, 'subcategory', e.target.value)}
                className="border border-gray-300 rounded-md p-1"
              />
              <br />
              {/* Input field for subsubcategory */}
              <label>Sub-subcategory:</label>
              <input
                type="text"
                value={product.subsubcategory}
                onChange={e => handleUpdateProductField(index, 'subsubcategory', e.target.value)}
                className="border border-gray-300 rounded-md p-1"
              />
              <br />
              {/* Input field for courseSchedule */}
              <label>Course Schedule:</label>
              <input
                type="text"
                value={product.courseSchedule}
                onChange={e => handleUpdateProductField(index, 'courseSchedule', e.target.value)}
                className="border border-gray-300 rounded-md p-1"
              />
              <br />
              {/* Input field for gradingPolicy */}
              <label>Grading Policy:</label>
              <input
                type="text"
                value={product.gradingPolicy}
                onChange={e => handleUpdateProductField(index, 'gradingPolicy', e.target.value)}
                className="border border-gray-300 rounded-md p-1"
              />
              <br />

              {/* Input fields for topicsCovered */}
              {product.topicsCovered && product.topicsCovered.map((topic, topicIndex) => (
                <>
                  <label>Topic {topicIndex + 1}:</label>
                  <input
                    key={topicIndex}
                    type="text"
                    value={topic}
                    onChange={(e) =>
                      handleUpdateTopic(topicIndex, e.target.value)
                    }
                    className="border border-gray-300 rounded-md p-1"
                  />
                  <br />
                </>
              ))}
              {/* Add Topic button */}
              <button onClick={() => handleAddTopic()} className="bg-gray-600 text-white px-4 py-2 rounded-md">Add Topic</button>
              <br />

              {/* Modules/Modules */}
              {product.modules && product.modules.map((module, moduleIndex) => (
                <>
                  {!editedModule || (editedModule.productIndex !== index || editedModule.moduleIndex !== moduleIndex) ? (
                    <>
                      {/* Module details */}
                      <h4>Module {moduleIndex + 1}</h4>
                      <p>Title: {module.title}</p>
                      <p>Content: {module.content}</p>
                      <p>Is Set Up: {module.isSetUp ? "Yes" : "No"}</p>
                      <p>Order: {module.order}</p>
                      {/* Edit Module button */}
                      <button onClick={() => setEditedModule({ productIndex: index, moduleIndex })} className="bg-gray-500 text-white px-4 py-2 rounded-md">
                        Edit Module
                      </button>
                      <button onClick={() => {
                        handleUpdateProductField(index, 'modules', 
                          (product as any).modules.filter((_: any, i: any) => i !== moduleIndex)
                        );
                      }}
                      className="bg-gray-500 text-white px-4 py-2 rounded-md">
                        Delete Module
                      </button>
                      <br />
                    </>
                  ) : (
                    <>
                      {/* Render input fields for each module field */}
                      <h4>Edit Module {moduleIndex + 1}</h4>
                      <label>Title:</label>
                      <input
                        type="text"
                        value={module.title}
                        onChange={(e) =>
                          handleUpdateModuleField(
                            index,
                            moduleIndex,
                            "title",
                            e.target.value
                          )
                        }
                        className="border border-gray-300 rounded-md p-1"
                      />
                      <br />
                      <label>Content:</label>
                      <input
                        type="text"
                        value={module.content}
                        onChange={(e) =>
                          handleUpdateModuleField(
                            index,
                            moduleIndex,
                            "content",
                            e.target.value
                          )
                        }
                        className="border border-gray-300 rounded-md p-1"
                      />
                      <br />
                      <label>Is Set Up:</label>
                      <input
                        type="checkbox"
                        checked={module.isSetUp}
                        onChange={(e) =>
                          handleUpdateModuleField(
                            index,
                            moduleIndex,
                            "isSetUp",
                            e.target.checked
                          )
                        }
                      />
                      <br />
                      <label>Module Order:</label>
                      <input
                        type="number"
                        value={module.order}
                        onChange={(e) =>
                          handleUpdateModuleField(
                            index,
                            moduleIndex,
                            "order",
                            parseInt(e.target.value)
                          )
                        }
                        className="border border-gray-300 rounded-md p-1"
                      />
                      <br />
                      {/* Done Editing Module button */}
                      <button onClick={() => setEditedModule(null)} className="bg-gray-500 text-white px-4 py-2 rounded-md">
                        Done Editing Module
                      </button>
              
                      {/* Sections */}
                      {module.sections && module.sections.map((section, sectionIndex) => (
                        <>
                          {!editedSection || (editedSection.productIndex !== index || editedSection.moduleIndex !== moduleIndex || editedSection.sectionIndex !== sectionIndex) ? (
                            <>
                              {/* Section details */}
                              <h5>Section {sectionIndex + 1}</h5>
                              <p>Title: {section.title}</p>
                              <p>Type: {section.type}</p>
                              <p>Order: {section.order}</p>
                              {section.type === 'video' && <p>Video URL: {section.data.url}</p>}
                              {section.type === 'content' && <p>Content stored</p>}
                              {section.type === 'questions' && <p>Questions stored</p>}
                              {/* Edit Section button */}
                              <button onClick={() => setEditedSection({ productIndex: index, moduleIndex, sectionIndex })} className="bg-gray-600 text-white px-4 py-2 rounded-md">
                                Edit Section
                              </button>
                              <button onClick={() => {
                                handleUpdateModuleField(index, moduleIndex, "sections", 
                                  (product as any).modules[moduleIndex].sections.filter((_: any, i: any) => i !== sectionIndex)
                                );
                              }}
                              className="bg-gray-600 text-white px-4 py-2 rounded-md">
                                Delete Section
                              </button>
                              <br />
                            </>
                          ) : (
                            <>
                              {/* Render input fields for each section field */}
                              <h5>Edit Section {sectionIndex + 1}</h5>
                              <label>Title:</label>
                              <input
                                type="text"
                                value={section.title}
                                onChange={(e) =>
                                  handleUpdateSection(
                                    index,
                                    moduleIndex,
                                    sectionIndex,
                                    "title",
                                    e.target.value
                                  )
                                }
                                className="border border-gray-300 rounded-md p-1"
                              />
                              <br />
                              {/* Only render the input fields for the currently edited section */}
                              {editedSection &&
                                editedSection.productIndex === index &&
                                editedSection.moduleIndex === moduleIndex &&
                                editedSection.sectionIndex === sectionIndex && (
                                  <>
                                    {section.type && 
                                      <>
                                        <label>Section Title:</label>
                                        <input
                                          type="text"
                                          value={section.title}
                                          onChange={(e) =>
                                            handleUpdateSection(
                                              index,
                                              moduleIndex,
                                              sectionIndex,
                                              "title",
                                              e.target.value
                                            )
                                          }
                                          className="border border-gray-300 rounded-md p-1"
                                        />
                                        <br />
                                        <label>Section Order:</label>
                                        <input
                                          type="number"
                                          value={section.order}
                                          onChange={(e) =>
                                            handleUpdateSection(
                                              index,
                                              moduleIndex,
                                              sectionIndex,
                                              "order",
                                              parseInt(e.target.value)
                                            )
                                          }
                                          className="border border-gray-300 rounded-md p-1"
                                        />
                                        <br />
                                      </>
                                    }
                                    <select
                                      value={editedSectionType || section.type}
                                      onChange={(e) => setEditedSectionType(e.target.value)}
                                    >
                                      <option value="">Select a type</option>
                                      <option value="video">Video</option>
                                      <option value="content">Content</option>
                                      <option value="questions">Questions</option>
                                    </select>
                                    {(editedSectionType || section.type) === 'video' && (
                                      <>
                                        <label>Video URL:</label>
                                        <input
                                          type="text"
                                          value={section.data.url}
                                          onChange={(e) =>
                                            handleUpdateSection(
                                              index,
                                              moduleIndex,
                                              sectionIndex,
                                              'data',
                                              { url: e.target.value }
                                            )
                                          }
                                          className="border border-gray-300 rounded-md p-1"
                                        />
                                        <br />
                                      </>
                                    )}
                                    {(editedSectionType || section.type) === 'content' && (
                                      <>
                                        {!showQuillEditor && (
                                          <button
                                            type="button"
                                            onClick={() => {
                                              handleEditContent(section.data.content);
                                              setShowQuillEditor(true);
                                            }}
                                          >
                                            Edit Content
                                          </button>
                                        )}
                                        {showQuillEditor && (
                                          <>
                                            <ReactQuill
                                              value={quillContent}
                                              onChange={setQuillContent}
                                              theme="snow"
                                              style={{ marginBottom: '100px' }}
                                            />
                                            <button
                                              type="button"
                                              onClick={() => {
                                                handleUpdateSection(
                                                  index,
                                                  moduleIndex,
                                                  sectionIndex,
                                                  'data',
                                                  { content: quillContent }
                                                );
                                                setShowQuillEditor(false);
                                              }}
                                            >
                                              Save Content
                                            </button>
                                          </>
                                        )}
                                      </>
                                    )}
                                    {(editedSectionType || section.type) === 'questions' && (
                                      <>
                                        {section.data.questions.map((question: any, questionIndex: number) => (
                                          <>
                                            {!editedQuestion || (editedQuestion.productIndex !== index || editedQuestion.moduleIndex !== moduleIndex || editedQuestion.sectionIndex !== sectionIndex || editedQuestion.questionIndex !== questionIndex) ? (
                                              <>
                                                {/* Question details */}
                                                <div key={questionIndex}>
                                                  <p>Question {questionIndex + 1}:</p>
                                                  <p>Type: {question.questionType}</p>
                                                  <p>Question: {question.question}</p>
                                                  {question.questionType === 'multipleChoice' && (
                                                    <>
                                                      <p>Choices:</p>
                                                      <ul>
                                                        {question.choices.map((choice: string, index: number) => (
                                                          <li key={index}>{choice}</li>
                                                        ))}
                                                      </ul>
                                                      <p>Correct Answers:</p>
                                                      <ul>
                                                        {question.correctAnswers.map((answer: string, index: number) => (
                                                          <li key={index}>{answer}</li>
                                                        ))}
                                                      </ul>
                                                    </>
                                                  )}
                                                  {question.questionType === 'text' && <p>Answer: {question.answer}</p>}
                                                </div>
                                                {/* Edit Question button */}
                                                <button onClick={() => setEditedQuestion({ productIndex: index, moduleIndex, sectionIndex, questionIndex })} className="bg-gray-800 text-white px-4 py-2 rounded-md">
                                                  Edit Question
                                                </button>
                                                <button onClick={() => {
                                                  handleUpdateSection(index, moduleIndex, sectionIndex, 'data', { 
                                                    questions: section.data.questions.filter((_: any, i: any) => i !== questionIndex) 
                                                  });
                                                }}
                                                className="bg-gray-800 text-white px-4 py-2 rounded-md">
                                                  Delete Question
                                                </button>
                                                <br />
                                              </>
                                            ) : (
                                              <>
                                                {/* Render input fields for editing the question */}
                                                {/* Input field for question type */}
                                                <label>Type:</label>
                                                <select
                                                  value={question.questionType}
                                                  onChange={(e) =>
                                                    handleUpdateSection(
                                                      index,
                                                      moduleIndex,
                                                      sectionIndex,
                                                      "data",
                                                      {
                                                        questions: section.data.questions.map((q: any, i: number) =>
                                                          i === questionIndex ? { ...q, questionType: e.target.value } : q
                                                        ),
                                                      }
                                                    )
                                                  }
                                                >
                                                  <option value="multipleChoice">Multiple Choice</option>
                                                  <option value="text">Text</option>
                                                </select>

                                                {/* Input field for question */}
                                                <label>Question:</label>
                                                <input
                                                  type="text"
                                                  value={question.question}
                                                  onChange={(e) =>
                                                    handleUpdateSection(
                                                      index,
                                                      moduleIndex,
                                                      sectionIndex,
                                                      "data",
                                                      {
                                                        questions: section.data.questions.map((q: any, i: number) =>
                                                          i === questionIndex ? { ...q, question: e.target.value } : q
                                                        ),
                                                      }
                                                    )
                                                  }
                                                />

                                                {/* Input fields for multiple choice questions */}
                                                {question.questionType === "multipleChoice" && (
                                                  <>
                                                    {/* Input fields for choices */}
                                                    {question.choices.map((choice: string, choiceIndex: number) => (
                                                      <>
                                                        {!editedChoice || (editedChoice.productIndex !== index || editedChoice.moduleIndex !== moduleIndex || editedChoice.sectionIndex !== sectionIndex || editedChoice.questionIndex !== questionIndex || editedChoice.choiceIndex !== choiceIndex) ? (
                                                          <>
                                                            {/* Choice details */}
                                                            <div key={choiceIndex}>
                                                              <p>Choice {choiceIndex + 1}:</p>
                                                              <p>{choice}</p>
                                                            </div>
                                                            {/* Edit Choice button */}
                                                            <button onClick={() => setEditedChoice({ productIndex: index, moduleIndex, sectionIndex, questionIndex, choiceIndex })} className="bg-gray-900 text-white px-4 py-2 rounded-md">
                                                              Edit Choice
                                                            </button>
                                                            <button onClick={() => {
                                                              handleUpdateSection(index, moduleIndex, sectionIndex, 'data', { 
                                                                questions: section.data.questions.map((q: any, i: any) => 
                                                                  i === questionIndex ? { ...q, choices: q.choices.filter((_: any, i: any) => i !== choiceIndex) } : q
                                                                ) 
                                                              });
                                                            }}
                                                            className="bg-gray-900 text-white px-4 py-2 rounded-md">
                                                              Delete Choice
                                                            </button>
                                                            <br />
                                                          </>
                                                        ) : (
                                                          <>
                                                            {/* Render input fields for editing the choice */}
                                                            <label>Choice:</label>
                                                            <input
                                                              type="text"
                                                              value={section.data.questions[questionIndex].choices[choiceIndex]}
                                                              onChange={(e) =>
                                                                handleUpdateSection(
                                                                  index,
                                                                  moduleIndex,
                                                                  sectionIndex,
                                                                  "data",
                                                                  {
                                                                    questions: section.data.questions.map((q: any, i: number) =>
                                                                      i === questionIndex
                                                                        ? {
                                                                            ...q,
                                                                            choices: q.choices.map((c: string, j: number) =>
                                                                              j === choiceIndex ? e.target.value : c
                                                                            ),
                                                                          }
                                                                        : q
                                                                    ),
                                                                  }
                                                                )
                                                              }
                                                            />

                                                            {/* Checkbox for correct answer */}
                                                            <input
                                                              type="checkbox"
                                                              checked={question.correctAnswers.includes(choice)}
                                                              onChange={(e) =>
                                                                handleUpdateSection(
                                                                  index,
                                                                  moduleIndex,
                                                                  sectionIndex,
                                                                  "data",
                                                                  {
                                                                    questions: section.data.questions.map((q: any, i: number) =>
                                                                      i === questionIndex
                                                                        ? {
                                                                            ...q,
                                                                            correctAnswers: e.target.checked
                                                                              ? [...q.correctAnswers, choice]
                                                                              : q.correctAnswers.filter(
                                                                                  (answer: string) => answer !== choice
                                                                                ),
                                                                          }
                                                                        : q
                                                                    ),
                                                                  }
                                                                )
                                                              }
                                                            />

                                                            {/* Done Editing Choice button */}
                                                            <button onClick={() => setEditedChoice(null)} className="bg-gray-900 text-white px-4 py-2 rounded-md">
                                                              Done Editing Choice
                                                            </button>
                                                          </>
                                                        )}
                                                      </>
                                                    ))}

                                                    {/* Add Choice button */}
                                                    <button
                                                      onClick={() =>
                                                        handleUpdateSection(
                                                          index,
                                                          moduleIndex,
                                                          sectionIndex,
                                                          "data",
                                                          {
                                                            questions: section.data.questions.map((q: any, i: number) =>
                                                              i === questionIndex ? { ...q, choices: [...q.choices, ""] } : q
                                                            ),
                                                          }
                                                        )
                                                      }
                                                      className="bg-gray-900 text-white px-4 py-2 rounded-md"
                                                    >
                                                      Add Choice
                                                    </button>
                                                  </>
                                                )}
                                                {/* Input field for text question answer */}
                                                {question.questionType === "text" && (
                                                  <>
                                                    <label>Answer:</label>
                                                    <input
                                                      type="text"
                                                      value={question.answer}
                                                      onChange={(e) =>
                                                        handleUpdateSection(
                                                          index,
                                                          moduleIndex,
                                                          sectionIndex,
                                                          "data",
                                                          {
                                                            questions: section.data.questions.map((q: any, i: number) =>
                                                              i === questionIndex ? { ...q, answer: e.target.value } : q
                                                            ),
                                                          }
                                                        )
                                                      }
                                                    />
                                                  </>
                                                )}

                                                {/* Done Editing Question button */}
                                                <button onClick={() => setEditedQuestion(null)} className="bg-gray-800 text-white px-4 py-2 rounded-md">
                                                  Done Editing Question
                                                </button>
                                              </>
                                            )}
                                          </>
                                        ))}
                                        {newQuestionData.questionType === 'multipleChoice' && (
                                          <>
                                            <label>Question:</label>
                                            <input
                                              type="text"
                                              onChange={(e) => handleUpdateNewQuestionData('question', e.target.value)}
                                              className="border border-gray-300 rounded-md p-1"
                                            />
                                            <br />
                                            <label>Choices:</label>
                                            {newChoices.map((choice, index) => (
                                              <>
                                                <input
                                                  key={index}
                                                  type="text"
                                                  value={choice}
                                                  onChange={(e) => handleUpdateChoice(index, e.target.value)}
                                                  className="border border-gray-300 rounded-md p-1"
                                                />
                                                <input
                                                  type="checkbox"
                                                  onChange={(e) => handleUpdateCorrectAnswers(choice, e.target.checked)}
                                                />
                                                <br />
                                              </>
                                            ))}
                                            <button onClick={handleAddChoice} className="bg-gray-900 text-white px-4 py-2 rounded-md">Add Choice</button>
                                            <br />
                                          </>
                                        )}
                                        {newQuestionData.questionType === 'text' && (
                                          <>
                                            <label>Question:</label>
                                            <input
                                              type="text"
                                              onChange={(e) => handleUpdateNewQuestionData('question', e.target.value)}
                                              className="border border-gray-300 rounded-md p-1"
                                            />
                                            <br />
                                            <label>Answer:</label>
                                            <input
                                              type="text"
                                              onChange={(e) => handleUpdateNewQuestionData('answer', e.target.value)}
                                              className="border border-gray-300 rounded-md p-1"
                                            />
                                            <br />
                                          </>
                                        )}
                                        {/* Add more input fields here for other question types */}
                                        {/* Add Question button */}
                                        <button onClick={() => {
                                          const newQuestion = {
                                            questionType: "",
                                            question: "",
                                            choices: [],
                                            correctAnswers: [],
                                            answer: "",
                                          };
                                          const newQuestions = [...section.data.questions, newQuestion];
                                          handleUpdateSection(
                                            index,
                                            moduleIndex,
                                            sectionIndex,
                                            'data',
                                            { questions: newQuestions }
                                          );
                                          setNewQuestionData({});
                                          setNewCorrectAnswers([]);
                                        }}
                                        className="bg-gray-800 text-white px-4 py-2 rounded-md">Add Question
                                        </button>
                                        <br />
                                      </>
                                    )}
                                  </>
                                )}
                              {/* Done Editing Section button */}
                              <button onClick={() => {
                                handleUpdateSection(
                                  index,
                                  moduleIndex,
                                  sectionIndex,
                                  'type',
                                  editedSectionType || section.type
                                );
                                setEditedSection(null);
                              }}
                              className="bg-gray-600 text-white px-4 py-2 rounded-md">
                                Done Editing Section
                              </button>
                            </>
                          )}
                        </>
                      ))}
                      {/* Add Section button */}
                      <button onClick={() => handleAddEditedSection(index, moduleIndex)} className="bg-gray-600 text-white px-4 py-2 rounded-md">Add Section</button>
              
                    </>
                  )}
                </>
              ))}
              {/* Add Module button */}
              <button onClick={() => handleAddEditedModule(index)} className="bg-gray-500 text-white px-4 py-2 rounded-md">
                Add Module
              </button>
              </div>
              ))}
              </div>
              {/* Submit button for Changes */}
              <button
                onClick={() => {
                  let allFieldsFilled = true;
                  editedProducts.forEach(product => {
                    if (!product.title || !product.price || !product.category || newProduct.image) {
                      allFieldsFilled = false;
                    }
                  });
                  if (allFieldsFilled) {
                    handleSubmitChanges();
                  } else {
                    alert('Please fill in all required fields');
                  }
                }}
                className="mt-auto button mx-auto mb-10"
              >
                Submit Changes
              </button>
              <br />
              <p>{editedProductsButtonText}</p>
      </main>
      ) : (
        // Render placeholder image for unauthorized users
        <img
          src="https://i.redd.it/70jk8jtyl6a31.jpg"
          alt="Move along, nothing to see here."
          style={{
            display: 'block',
            maxWidth: '80%',
            maxHeight: '80%',
            width: 'auto',
            height: 'auto',
            position: 'absolute',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)'
          }}
        />
      )}
    </div>
  );
};

export default Populate;

export const getServerSideProps = async (
  context: GetServerSidePropsContext
) => {
  // Get user logged in credentials
  const session: ISession | null = await getSession(context);
  return {
    props: {
      session,
    },
  };
};