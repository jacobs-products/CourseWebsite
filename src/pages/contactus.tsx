import React from 'react';
import Head from 'next/head';
import Header from '../components/Header';

const Contact = () => {
  return (
    <div className="bg-white min-h-screen">
      <Head>
        <title>LinguaPro</title>
      </Head>
      <Header />
      <br />
      <div className="flex flex-col items-center">
        <iframe
          src="https://docs.google.com/forms/d/e/1FAIpQLSfkZ8rxMLEwg_pOGsiEbmZ7yXfx_D_OKuK1ml9V_BZmxUv-3A/viewform?embedded=true"
          className="responsive-iframe"
        >
          Loading…
        </iframe>
      </div>
      <style jsx>{`
        .responsive-iframe {
          width: 80%;
          max-width: 640px;
          height: 100vh;
          max-height: 875px;
        }
  
        @media screen and (max-width: 768px) {
          .responsive-iframe {
            width: 80%;
            height: 80vh;
          }
        }
      `}</style>
    </div>
  );  
};

export default Contact;