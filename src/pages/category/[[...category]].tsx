import { useRouter } from 'next/router';
import { IProduct } from '../../../typings';
import Header from '../../components/Header';
import ProductFeed from '../../components/ProductFeed';
import React from 'react';
import { useProductContext } from '../../components/context/ProductContext';

const CategoryPage = () => {
  const router = useRouter();
  const { category: categorySegments } = router.query;
  const { products, loading, error } = useProductContext();

  // Get the category, subcategory, and subsubcategory values from the URL path
  const [category, subcategory, subsubcategory] = categorySegments || [];

  // Filter the list of products based on the category, subcategory, and subsubcategory
  const filteredProducts = products.filter(product => {
    if (subsubcategory) {
      return product.subsubcategory === subsubcategory && product.subcategory === subcategory && product.category === category;
    } else if (subcategory) {
      return product.subcategory === subcategory && product.category === category;
    } else {
      return product.category === category;
    }
  });

  // Reverse the order of the products
  const reversedProducts = [...filteredProducts].reverse();

  return (
    <div className="bg-white min-h-screen">
      <Header />
      <main className="max-w-screen-2xl mx-auto p-4 md:p-8">
        <h1 className="text-3xl font-bold mt-4 mb-6 text-center text-gray-800">
          {subsubcategory || subcategory || category}
        </h1>
        {/* Pass the reversed list of products to the ProductFeed component */}
        <ProductFeed products={reversedProducts} />
      </main>
    </div>
  );  
};

export default CategoryPage;