import React, { useState, useRef } from 'react';
import ReactPlayer from 'react-player';
import { useRouter } from 'next/router';
import { useProductContext } from '../../components/context/ProductContext';
import Header from 'components/Header';
import dynamic from 'next/dynamic';
import { useSession } from 'next-auth/react';
import { useFetchOrders } from 'hooks/UseFetchOrders';
import { useOrderContext } from 'components/context/OrderContext';


const CourseContent = () => {
  const [playing, setPlaying] = useState(false);
  const router = useRouter();
  const { title: courseTitleValue } = router.query;
  const { products } = useProductContext();
  const courseTitle = decodeURIComponent(courseTitleValue as string);

  const filteredCourses = products.filter(
    (product) => product.title.toLowerCase() === (courseTitle ? courseTitle.toLowerCase() : '')
  );
  const [userAnswers, setUserAnswers] = useState({});
  const [score, setScore] = useState<number | null>(null);
  const colors = ['#F0F0F0', '#D9E4DD', '#DDDCC3', '#B3B4B9', '#899da3'];

  const course = filteredCourses[0];

  const playerRef = useRef(null);

  const handlePlayClick = () => {
    if (playerRef && playerRef.current) {
      setPlaying(true);
    }
  };

  const { data: session } = useSession();
  useFetchOrders(session);
  const { orders } = useOrderContext();
  let hasPurchasedCourse = orders.some(order => order.title.includes(courseTitle));

  const authorizedEmails = (process.env.NEXT_PUBLIC_AUTHORIZED_EMAIL as any).includes(",")
    ? (process.env.NEXT_PUBLIC_AUTHORIZED_EMAIL as any).split(",")
    : [process.env.NEXT_PUBLIC_AUTHORIZED_EMAIL];

  if (authorizedEmails.includes(session?.user.email as string)) {
    hasPurchasedCourse = true;
  }

  return (
    <div className="bg-white min-h-screen">
      <Header />
      <main className="max-w-screen-2xl mx-auto p-4 md:p-8">
      {!hasPurchasedCourse ? (
          <p>You have not purchased this course.</p>
        ) : (!course || !courseTitle) ? (
          <p>Loading...</p>
        ) : (
          <>
            <h1 className="text-3xl font-bold mt-4 mb-6 text-center text-gray-800">
              {course.title}
            </h1>
            {course.modules?.map((module, i) => (
              module.isSetUp && (
                <details key={i} className="p-4 my-4 rounded-lg" style={{ backgroundColor: colors[i % colors.length] }}>
                  <summary className="text-xl font-semibold cursor-pointer">{module.title}</summary>
                  <p className="mt-2">{module.content}</p>
                  {module.sections?.map((section, j) => (
                    <details key={j} style={{ backgroundColor: colors[i % colors.length] }} className="p-4 my-4">
                      <summary className="text-lg font-medium cursor-pointer">{section.title}</summary>
                      {section.type === "video" ? (
                        <div className="video-wrapper" onClick={handlePlayClick}>
                          <ReactPlayer
                            ref={playerRef}
                            url={section.data.url}
                            controls={true}
                            playing={playing}
                            light={true}
                            style={{ position: 'absolute', top: 0, left: 0 }}
                            width="100%"
                            height="100%"
                            config={{
                              youtube: {
                                playerVars: { modestbranding: 1, rel: 0, origin: window.location.origin }
                              }
                            }}
                          />
                        </div>
                      ) : section.type === "content" ? (
                        <div
                          className="prose max-w-none"
                          dangerouslySetInnerHTML={{
                            __html: section.data.content
                              .replace(/<h1>/g, '<h1 style="font-size: 2em; margin: 0.67em 0;">')
                              .replace(/<h2>/g, '<h2 style="font-size: 1.5em; margin: 0.83em 0;">')
                              .replace(/<h3>/g, '<h3 style="font-size: 1.17em; margin: 1em 0;">')
                              .replace(/<h4>/g, '<h4 style="margin: 1.33em 0;">')
                              .replace(/<h5>/g, '<h5 style="font-size: 0.83em; margin: 1.67em 0;">')
                              .replace(/<h6>/g, '<h6 style="font-size: 0.67em; margin: 2.33em 0;">')
                              .replace(/<p>/g, '<p style="margin: 1em 0;">')
                              .replace(/<ul>/g, '<ul style="list-style-type: disc; margin: 1em 0; padding-left: 40px;">')
                              .replace(/<ol>/g, '<ol style="list-style-type: decimal; margin: 1em 0; padding-left: 40px;">'),
                          }}
                        />
                      ) : section.type === "questions" ? (
                        <>
                          {section.data.questions.map((question: any, k: any) => (
                            <div key={k}>
                              <p>{question.question}</p>
                              {question.questionType === "multipleChoice" ? (
                                <>
                                  {question.choices.map((choice: any, l: any) => (
                                    <div key={l}>
                                      {question.correctAnswers.length > 1 ? (
                                        <input
                                          type="checkbox"
                                          name={`question-${k}`}
                                          id={`choice-${l}`}
                                          onChange={(e) =>
                                            setUserAnswers((prevAnswers) => {
                                              const prevAnswer = (prevAnswers as any)[k] || [];
                                              if (e.target.checked) {
                                                return {
                                                  ...prevAnswers,
                                                  [k]: [...prevAnswer, choice],
                                                };
                                              } else {
                                                return {
                                                  ...prevAnswers,
                                                  [k]: prevAnswer.filter((a: any) => a !== choice),
                                                };
                                              }
                                            })
                                          }
                                          disabled={score !== null}
                                        />
                                      ) : (
                                        <input
                                          type="radio"
                                          name={`question-${k}`}
                                          id={`choice-${l}`}
                                          onChange={() =>
                                            setUserAnswers((prevAnswers) => ({
                                              ...prevAnswers,
                                              [k]: choice,
                                            }))
                                          }
                                          disabled={score !== null}
                                        />
                                      )}
                                      <label htmlFor={`choice-${l}`}>{choice}</label>
                                    </div>
                                  ))}
                                </>
                              ) : question.questionType === "text" ? (
                                <>
                                  <input
                                    type="text"
                                    onChange={(e) =>
                                      setUserAnswers((prevAnswers) => ({
                                        ...prevAnswers,
                                        [k]: e.target.value,
                                      }))
                                    }
                                    disabled={score !== null}
                                  />
                                </>
                              ) : null}
                            </div>
                          ))}
                          {/* Submit button */}
                          {score === null && (
                            <button
                              onClick={() => {
                                // Calculate the score
                                let correctAnswers = 0;
                                section.data.questions.forEach((question: any, k: any) => {
                                  const userAnswer = (userAnswers as any)[k] || [];
                                  if (question.questionType === "multipleChoice") {
                                    if (question.correctAnswers.length > 1) {
                                      // Check if all correct answers are selected
                                      if (
                                        userAnswer.length === question.correctAnswers.length &&
                                        userAnswer.every((a: any) => question.correctAnswers.includes(a))
                                      ) {
                                        correctAnswers++;
                                      }
                                    } else if (question.correctAnswers.includes(userAnswer)) {
                                      correctAnswers++;
                                    }
                                  } else if (question.questionType === "text") {
                                    if ((userAnswers as any)[k] === question.answer) {
                                      correctAnswers++;
                                    }
                                  }
                                });
                                setScore(correctAnswers);
                                setUserAnswers({});
                              }}
                            >
                              Submit
                            </button>
                          )}
                          {/* Score */}
                          {score !== null && (
                            <p>
                              You got {score} out of {section.data.questions.length} questions right!
                            </p>
                          )}
                        </>
                      ) : null}
                    </details>
                  ))}
                </details>
              )
            ))}
          </>
        )}
      </main>
      <style jsx>{`
        .video-wrapper {
          position: relative;
          padding-top: 56.25%;  /* 16:9 Aspect Ratio */
          height: 0;  /* Collapse div to zero height; will be filled by padding */
          overflow: hidden;
        }
        .video-wrapper > :global(.react-player) {
          position: absolute;
          top: 0;
          left: 0;
          width: 100%;
          height: 100%;
        }
      `}</style>
    </div>
  );
};

export default CourseContent;