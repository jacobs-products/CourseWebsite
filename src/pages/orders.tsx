import { GetServerSidePropsContext } from "next";
import { getSession, useSession } from "next-auth/react";
import { ISession } from "../../typings";
import Header from "../components/Header";
import Order from "../components/Order";
import { useOrderContext } from '../components/context/OrderContext';
import { useFetchOrders } from '../hooks/UseFetchOrders';

type Props = {};

const Orders = ({}: Props) => {
  const { data: session } = useSession();
  useFetchOrders(session);
  const { orders, error } = useOrderContext();

  if (error) {
    return <p>{error}</p>;
  }

  return (
    <div className="bg-white min-h-screen">
      <Header />
      <main className="max-w-screen-l max-auto p-10">
        <h1 className="text-3xl border-b mb-2 pb-1 border-[fast_blue] text-gray-800">
          Your Orders
        </h1>
        {session ? (
          <>
            <h2 className="text-gray-800">{orders.length} Orders</h2>
            <div className="mt-5 space-y-4">
              {orders?.map((order) => (
                <Order key={order.id} order={order} />
              ))}
            </div>
          </>
        ) : (
          <h2 className="text-gray-800">Please sign in to see your orders</h2>
        )}
      </main>
    </div>
  );  
};

export default Orders;

export const getServerSideProps = async (
  context: GetServerSidePropsContext
) => {
  // Get user logged in credentials
  const session: ISession | null = await getSession(context);

  return {
    props: {
      session,
    },
  };
};
