import React from 'react';
import Head from 'next/head';
import Header from '../components/Header';

type Props = {};

const About = (props: Props) => {
  return (
    <>
      <Head>
        <title>About | LinguaPro</title>
      </Head>
      <Header />
      <main className="bg-white min-h-screen">
        <div className="max-w-screen-2xl mx-auto p-10">
          <h1 className="text-3xl font-bold mb-6 text-gray-800">About Me</h1>
          <p className="text-lg mb-4 text-gray-800">
            Marhaba! My name is May Saffar. I have over 20 years of diverse, extensive teaching experience in the United States. I hold a TESOL Masters degree from SUNY, Albany and taught students grades K- adults.
          </p>
          <p className="text-lg mb-4 text-gray-800">
            Teaching is what defines me, and remains the second most rewarding job for me. If you are curious what’s the first one, it’s attending to my role as a mother.
          </p>
          <p className="text-lg mb-4 text-gray-800">
            I am a proud mother of three children:)
          </p>
          <p className="text-lg mb-4 text-gray-800">
            Other than teaching, I like to swim, walk, dance, cook, travel and learn more about spirituality as well as cognitive science.
          </p>
          <p className="text-lg mb-4 text-gray-800">
            My Arabic online courses are meant to be for absolute beginners. Then they progress to include the next proficiency levels of learners such as intermediate and advanced.
          </p>
          <p className="text-lg mb-4 text-gray-800">
            TPRS stands for Teaching Proficiency through Reading and Storytelling is one of the best approaches of language teaching. TPRS is found by Blaine Ray in the 1990’s and it’s one fun way of learning a new language. You will read silly/funny and abrupt stories, followed by many comprehension questions about the story.
          </p>
          <p className="text-lg mb-4 text-gray-800">
            Transliteration is the initial method I use to teach you Arabic, while I also teach you the Arabic alphabet as separate lessons so you gain balanced and appropriate learning of the Arabic language.
          </p>
          <p className="text-lg mb-4 text-gray-800">
            The first story is the seed, then all other stories stem from the story seed.
          </p>
          <p className="text-lg mb-4 text-gray-800">
            I promise you will learn how to speak Arabic from the very first lesson. So let’s get to work!
          </p>
        </div>
      </main>
    </>
  );
};

export default About;

