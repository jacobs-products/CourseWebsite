export interface IModule {
  title: string;
  content: string;
  isSetUp: boolean;
  sections?: ISection[];
  order: number;
}

export interface ISection {
  title: string;
  type: string;
  data: any;
  order: number;
}

export interface IProduct {
  id: string;
  title: string;
  price: number;
  description?: string;
  category: string;
  subcategory?: string;
  subsubcategory?: string;
  image: string;
  quantity: number;
  courseSchedule?: string;
  modules?: IModule[];
  gradingPolicy?: string;
  topicsCovered?: string[];
}

export interface IOrder {
  id: number;
  title: string;
  amount: number;
  amount_shipping: number;
  timestamp: number;
  images: string[];
}

export interface ISession {
  user: {
    name: string;
    email: string;
    image: string;
    address: string;
  } & DefaultSession["user"];
  expires: string;
}

export type Blog = {
  id: number;
  title: string;
  author: string;
  content: string;
  date: string;
};